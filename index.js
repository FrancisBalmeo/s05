// Mathematical Operations (-, *, /, %) 
// subtraction
let numString1 = "5"; 
let numString2 = "6"; 
let num1 = 5; 
let num2 = 6; 
let num3 = 5.5; 
let num4 = .5;

console.log(num1-num3); //-0.5 result 
console.log(num3-num4)//5
console.log(numString1 - num2);//-1
console.log(numString2 - num2);
console.log(numString1 - numString2); // in Subtraction numeric strings will not concatenate and instead will forcibly change its type.

let sample2 = "Huan Dela Cruz";
console.log(sample2 - numString1); //NaN - results is not a number. Performing subraction with numeric and alphanumeric strings will result to NaN

//Multiplication
console.log(num1*num2); //30
console.log(numString1*num1);//25
console.log(numString1*numString2);//30
console.log(sample2* 5);

let product = num1 * num2; 
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log(product);

//Division
console.log(product/num2);//5
console.log(product2/5);//5
console.log(numString2/numString1);//1.2
console.log(numString2 % numString1);//1

// division/multiplication by 0
console.log(product2 * 0);
console.log(product3 / 0);

//Division by 0 in JS it results to infinity, or undefined.

// Modulo % - remainder of division operation
console.log(product2 % num2); //25/6 = remainder  1
console.log(product3 % product2); //30/25 = remainder 5
console.log(num1%num2);//5
console.log(num1%num1);//0 

// Boolean (true or false)
/*
	Boolean is usually used for logic operations or if-else condition
		-boolean values are noramlly used to store values relating to the state of certain things
	When creating a which will contain a boolean, the variable name is usually a yes or no question.
*/

	let isAdmin = true;
	let isMarried = false;
	let isMVP = true;

	// You can also concatenate strings + boolean
	console.log("Is she married? "+isMarried);
	console.log("Is he the MVP? "+ isMVP);
	console.log(`Is he the current admin? `+isAdmin);

//Arrays 
/*
	Arrays
	- are a special kind of data type to store multiple values
	- can actually store data with different types in JS, but the best practice arrays are used to contain multitple values with the same data types.
	- values in an arrays are separated by commas
	An array is created with an array literal = []
*/
// Syntax:
	//let/const arrayName = [elementA, elementB, elementC];
let array1 = ["Goku","Picolo","Majinbu","Vegeta"];
console.log(array1);
let array2 = ["One Punch Man", true, 50000, "Saitama"];
console.log(array2);


let grade = [98.7, 92.1, 90.2, 94.6];
console.log(grade);
// Arrays are better thought of as group data

//Objects
/*
	Objects
		- are special data types, used to mimic the real world
		- used to create complex data that contain pieces of information that are very relevant to each other
		- Objects are created with object literals = {}
			- each data/value are paired with a key
			- each field is called a property
			- each field is separated by a comma

			syntax:
				let/const objectName = {
					propertyA: value,
					propertyB: value
				}
*/

let person = {
	fullName: "John Francis Balmeo",
	age: 20,
	isMarried: false,
	contact: ["+0465069812"],
	address:{
		houseNumber: '345',
		street: 'Diamond',
		city: 'Dasamarinas'
	}
};

console.log(person);

/*Mini-Activity

create a variable with a group of data
	-the group of data should containt names from your favorite band/idol


create a variable which contain multiple values of differing types, and describe a single person.
	-this data should be able to contain multiple key value pairs:
		firstName:<Value>,
		lastName:<Value>,
		isDeveloper:<Value>,
		hasPortfolio:
		age:
		contact:
		address:
			houseNumber:
			street:
			city:


*/

let favorite_band = ["Winter", "NingNing", "Karina", "Giselle"];

let single_person = {
	firstName: "John Francis",
	lastName: "Balmeo",
	isDeveloper: false,
	hasPortfolio: false,
	age: 20,
	contact:["House: 0465069812", "Emergency(Father): 09178902943"],
	address:{
		houseNumber: "Blk10 Lt8 Plaza De San Marino",
		street: "N/A",
		city: "Salawag, Dasamarinas"
	}
};

console.log(favorite_band);
console.log(single_person);

// Undefine vs Null
		//Null - is explicit absence of data/value this is done to project that a variable contains nothing over undefined
		//Undefined merely means there is no data in the variable because the variable has not been assigned an initial value 

		let sampleNULL = null;
		console.log(sampleNULL);

		//Undefined - is a representation that a variable has been declared but it was not assigned an initial value

		let sampleUndefined;
		console.log(sampleUndefined);

		//certain processes in programming explicitly return null  to indicate that the task resulted to nothing 

		let foundResult = null;
		//Example:
		let myNumber = 0;
		let myString = "";
		// using null to compare to a 0 value and an empty string is much better for readability

		//for undefined, normally caused by developers creating variables that have no values or data associated with them.
		//this is when a variable does exist but its values is unknown.

		let person2 ={
			name: "Peter Parker",
			age: 28,
		}
		//undefined because person2 does exist however but the property isAdmin does not exist
		console.log(person2.isAdmin);

		/* [SECTION] functions
		Functions 
			- lines/block of codes that tell our device/application to perform a certain task when called/invoked
			- are reusable pieces of code with instructions which can be used multiple times as long as we call/invoke them

			syntax:
			function functionName(parameters){
				code block
					- block of code that will be executed once the function has been run/called/invoked
			}	
		*/
//Declaring the function
function printName1(){
	console.log("My name is Francis");
}
//calling
printName1();
printName1();
printName1();

function showSum(){
	console.log(25+6);
}
showSum();
//Note: do not create functions with the same name

/*Parameters and Arguments*/
//parameters are used to used to store information. The value passed to the function as an argument.

function printName(name){
	console.log(`My name is ${name}`);
}
// When a function is called and data is passes, we call that data as argument.
// In this call, Rin is an argument into our printName function
//data passes into a function: argument
//representation of the argument within the function: parameter
//printName("Rin");

/*function displayNum(num){
	alert(num); 
}

displayNum(30);
*/

/* Mini-Activity
create a function that will be able to show message in the console.
	The message be able to passed into function via argument.
	- Sample Message:
		"JavaScript is Fun"
		"C# is Fun"

		argument: programming language
*/

function programming(language){
	console.log(`${language} is Fun`);
}
programming("JavaScript");
programming("Java");
programming("C++");
programming("Python");

// Multiple parameters and arguments

function displayFullName(firstName, mi, lastName, age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
}

displayFullName("John Francis", "G.", "Balmeo", 20);

// return keyword
	// The "return" statement allows the output of a function to be passed to the kine/block of code that invoked/called the function
	//any line of code that comess after the return statement is ignored because it ends the function execution

	function createFullName (firstName, middleName, lastName){

		//return `${firstName} ${middleName} ${lastName}`
		return firstName + middleName + lastName
	}

	console.log(createFullName("John Francis ", "Garcia ", "Balmeo"));

